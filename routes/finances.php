<?php

Route::get('/finances/overview', 'Finances\ProjectController@index');

Route::get('/finances/project/{project}', 'Finances\ProjectController@project');