<?php

Route::get('/profile', 'ProfileController@index');

Route::post('/profile', 'ProfileController@update');