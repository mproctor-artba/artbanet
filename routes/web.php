<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/services/twitter/{hash}', 'PublicController@twitter');
Route::get('/services/tweets/{hash}', 'PublicController@tweets');

Route::post('/sharepoint/login', 'SharePointController@index');

Route::post('/sharepoint/validate', 'SharePointController@signin');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/logout', 'Auth\LoginController@logout');

include('profile.php');

include('finances.php');

include('analytics.php');
