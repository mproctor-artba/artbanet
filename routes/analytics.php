<?php

Route::get('/analytics/overview', 'Analytics\ProjectController@index');

Route::get('/analytics/project/{project}', 'Analytics\ProjectController@project');