@extends('layouts.application')

@section('css')
    <!-- DataTables -->
    <link href="/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">{{ $title }}</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL('/') }}">Home</a></li>
                <li class="breadcrumb-item">Project Finances</li>
                <li class="breadcrumb-item active">{{ $project }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="profile-detail card-box">
                <div>
                	<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Amount</th>
                                <th>Reason</th>
                                <th>Status</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transactions->data as $tx)
                                @if($project == "SCTPP")
                                    @if($tx->description == "Safety Certification Application" || $tx->description == "Safety Certification Testing Fee")
                                        <tr>
                                            <td>${{ number_format($tx->amount/100, 2) }}</td>
                                            <td class="text-left"><?= $tx->description; ?></td>
                                            <td>{{ ucfirst($tx->status) }}</td>
                                            <td>{{ date("m-d-Y", $tx->created) }}</td>
                                        </tr>
                                    @endif
                                @endif
                                @if($project == "OLC")
                                    @if($tx->description != "Safety Certification Application" && $tx->description != "Safety Certification Testing Fee")
                                        <tr>
                                            <td>${{ number_format($tx->amount/100, 2) }}</td>
                                            <td class="text-left"><?= $tx->description; ?></td>
                                            <td>{{ ucfirst($tx->status) }}</td>
                                            <td>{{ date("m-d-Y", $tx->created) }}</td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Required datatable js -->
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/plugins/datatables/jszip.min.js"></script>
    <script src="/plugins/datatables/pdfmake.min.js"></script>
    <script src="/plugins/datatables/vfs_fonts.js"></script>
    <script src="/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/plugins/datatables/buttons.print.min.js"></script>
    <script src="/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            //Buttons examples
            var table = $('#datatable-buttons').DataTable({
                lengthChange: false,
                iDisplayLength: 25,
                buttons: ['copy', 'excel', 'pdf']
            });

            table.buttons().container()
                    .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
        } );

    </script>
@endsection