@extends('layouts.application')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Profile</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL('/') }}">Home</a></li>
                <li class="breadcrumb-item active">Profile</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="profile-detail card-box">
                <div>
                	<form method="POST" class="text-left" action="{{ URL('/profile') }}">
                		{{ CSRF_FIELD() }}
                		<div class="form-group">
                			<label class="form-label">First Name</label>
                			<input type="text" name="fname" class="form-control" value="{{ Auth::user()->f_name }}"> 
                		</div>
                		<div class="form-group">
                			<label class="form-label">Last Name</label>
                			<input type="text" name="lname" class="form-control" value="{{ Auth::user()->l_name }}"> 
                		</div>
                		<div class="form-group">
                			<label class="form-label">Title</label>
                			<input type="text" name="title" class="form-control" value="{{ Auth::user()->title }}"> 
                		</div>
                		<div class="form-group">
                			<label class="form-label"></label>
                			<input type="submit" class="btn btn-success submitbutton" value="Save Profile"> 
                		</div>
                	</form>
                </div>
            </div>
        </div>
    </div>
@endsection