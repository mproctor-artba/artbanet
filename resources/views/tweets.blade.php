@extends('layouts.app')

@section('css')
<style type="text/css">.navbar{display:none !important;}.profile-image{width: 50px; display:block; margin: 0 auto; border-radius:50%;}</style>
@endsection

@section('content')
	<div class="row" style="margin: 25px 0;">
		<div class="col-md-8">
			<div id="tweets"></div>
		</div>
		<div class="col-md-3" style="text-align: center;">
			<img src="https://static.wixstatic.com/media/1e3b76_a84d25c4fad749fd8b14210d1105ce59~mv2.png/v1/fill/w_88,h_82,al_c,usm_0.66_1.00_0.01/1e3b76_a84d25c4fad749fd8b14210d1105ce59~mv2.png" width="200">
			<h1>Tweet #ACEDC</h1>
			<h3>and join the conversation!</h3>
		</div>
	</div>
    
@endsection

@section('js')
<script type="text/javascript">
jQuery(document).ready(function($) {

	get_tweets();

function get_tweets(){
    var feedback = $.ajax({
        type: "GET",
        url: "/services/tweets/{{ $hash }}",
        async: false,
        success: function(feedback){
        	$('#tweets').html(feedback);
	        setTimeout(function(){get_tweets();}, 2000);
	    }
	});

    
}


});
</script>

@endsection