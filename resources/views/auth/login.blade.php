@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}" id="signin">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-primary" id="login">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script>

        $('body').keydown(function (e){
            if(e.keyCode == 13){
                var email = $("#email").val();
                var pass = $("#password").val();
                login(email, pass);
            }
        });

        $("#login").click(function(){
            var email = $("#email").val();
            var pass = $("#password").val(); 
            login(email, pass);
        });

        function login(email, pass){
            $("#login").text("Logging in...");
            $("#login").toggleClass("btn-primary");
            var email = $("#email").val();
            var pass = $("#password").val();

            $.post( "{{ URL('/sharepoint/login/') }}", $( "#signin" ).serialize(), function( data ) {
              if(data == "ok"){
                validate(email, pass);
              }
            });
        }

        function validate(email, pass){
            $.post( "{{ URL('/sharepoint/validate') }}/", $( "#signin" ).serialize(), function( data ) {
              if(data == "ok"){
                $("#signin").submit();
              }
            });
        }
    </script>
@endsection
