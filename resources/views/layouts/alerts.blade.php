@if(session('success') != null)
	{{ bsAlert("Success!", session('success'), "success") }}
@endif
@if(session('error') != null)
	{{ bsAlert("Something went wrong...", session('error'), "error") }}
@endif