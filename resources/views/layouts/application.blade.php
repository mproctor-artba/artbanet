
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon_1.ico">

        <title>Ubold - Responsive Admin Dashboard Template</title>

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="/plugins/morris/morris.css">

        <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="/assets/js/modernizr.min.js"></script>
        @yield('css')
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            @include('layouts.header')

            @include('layouts.navigation')

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        @yield('content')

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <?= Date('Y'); ?> American Road & Transportation Builders Association
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

            @include('layouts.shelf')
            

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/detect.js"></script>
        <script src="/assets/js/fastclick.js"></script>
        <script src="/assets/js/jquery.slimscroll.js"></script>
        <script src="/assets/js/jquery.blockUI.js"></script>
        <script src="/assets/js/waves.js"></script>
        <script src="/assets/js/wow.min.js"></script>
        <script src="/assets/js/jquery.nicescroll.js"></script>
        <script src="/assets/js/jquery.scrollTo.min.js"></script>

        <script src="/plugins/peity/jquery.peity.min.js"></script>

        <!-- jQuery  -->
        <script src="/plugins/waypoints/lib/jquery.waypoints.min.js"></script>
        <script src="/plugins/counterup/jquery.counterup.min.js"></script>

        <script src="/plugins/notifyjs/js/notify.js"></script>
        <script src="/plugins/notifications/notify-metro.js"></script>

        <script src="/assets/js/jquery.core.js"></script>
        <script src="/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                @include('layouts.alerts')

                $(".submitbutton").click(function(){
                    $(this).text("Submitting data...");
                    $(this).val("Submitting data...");
                    $(this).removeClass("btn-primary");
                    $(this).removeClass("btn-success");
                });

                $("#btn-fullscreen").click(function(){
                    $(this).toggleClass("active");
                })
            });
        </script>

        @yield('js')

    </body>
</html>
