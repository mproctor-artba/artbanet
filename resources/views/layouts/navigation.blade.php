<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="{{ URL('/') }}" class="waves-effect @if($page == 'home') active @endif"><i class="ti-home"></i> <span> Dashboard </span></a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect @if($page == 'finances') active @endif @if($page == 'finances') && $subpage != NULL) subdrop @endif"><i class="ti-money"></i> <span> Finances </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{ URL('/finances/overview') }}" @if($subpage == 'finover') active @endif>Overview</a></li>
                        <li><a href="{{ URL('/finances/project/sctpp') }}" @if($subpage == 'sctpp') active @endif>SCTPP</a></li>
                        <li><a href="{{ URL('/finances/project/olc') }}" @if($subpage == 'finover') active @endif>Online Learning Center</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect @if($page == 'webanalytics') active @endif @if($page == 'webanalytics' && $subpage != NULL) subdrop @endif"><i class="ti-pulse"></i> <span> Web Analytics </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                    <li><a href="{{ URL('/analytics/project/ARTBA') }}" @if($subpage == 'artbaax') active @endif>ARTBA</a></li>
                        <li><a href="{{ URL('/analytics/project/sctpp') }}" @if($subpage == 'sctppax') active @endif>SCTPP</a></li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->