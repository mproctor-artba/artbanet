<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;
use Carbon\Carbon;
use App\User as User;
use App\Connection as Connections;

function formatGMTstring($date, $format){
    $date =  new DateTime($date);
    return $date->format($format);
}

function dateToISO($date){
    $date =  new DateTime($date);
    return $date->format("c");
}

function DateDifference($start, $end){
    $datetime1 = new DateTime($start);
    $datetime2 = new DateTime($end);
    $interval = $datetime1->diff($datetime2);
    return $interval->format('%R%a');
}

function getFullName($id){
    $user = User::where('id', $id)->first();
    return $user->f_name . " " . $user->l_name;
}

function convertHTMLTime($time)
{
    return date("m-d-Y", strtotime($time));
}

function convertTimestamp($stamp)
{
    return date('M j Y g:i A', strtotime($stamp));
}

function convertHTMLTime2($date){
    $parts = explode('-',$date);
    $yyyy_mm_dd = $parts[2] . '-' . $parts[0] . '-' . $parts[1];
    return $yyyy_mm_dd;
}

function exposeArray($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function bsAlert($title, $alert, $class){
    $icon = "check";
    if($class == "error"){
        $icon = "exclamation";
    }
    echo '
    $.notify({
                    title: "' . $title . '",
                    text: "' . $alert . '",
                    image: "<i class=\'fa fa-' . $icon . '\'></i>"
                }, {
                    style: "metro",
                    className: "' . $class . '",
                    showAnimation: "show",
                    showDuration: 0,
                    hideDuration: 0,
                    autoHideDelay: 4000,
                    autoHide: true,
                    clickToHide: true
                });
    ';
}