<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Thybag\SharePointAPI;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            "title" => "ARTBA Dashboard",
            "page" => "home",
            "subpage" => NULL
        ];
        
        return view('home', $data);
    }
}
