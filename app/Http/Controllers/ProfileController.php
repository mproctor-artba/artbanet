<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;
use Auth;

class ProfileController extends Controller
{
    public function index(){
    	$data = [
            "title" => "ARTBA Profile Manager",
            "page" => "profile",
            "subpage" => NULL
        ];

    	return view('profile.index', $data);
    }

    public function update(Request $request){
    	DB::table('users')->where('id', Auth::user()->id)->update([
    		"f_name" => $request->fname,
    		"l_name" => $request->lname,
    		"title" => $request->title
    	]);

    	Session::flash('success', "You've updated your profile");

    	return Redirect::to('/profile');
    }
}
