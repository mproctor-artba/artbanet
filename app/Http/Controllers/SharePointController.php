<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

require_once(base_path('/vendor/vgrem/php-spo/examples/bootstrap.php'));
require_once(base_path('/vendor/vgrem/php-spo/src/SharePoint/UserProfiles/PeopleManager.php'));
use Office365\PHP\Client\Runtime\Auth\AuthenticationContext;
use Office365\PHP\Client\SharePoint\ClientContext;
use Office365\PHP\Client\Runtime\ClientRuntimeContext;

use Auth;
use App\User as Users;
use DB;

class SharePointController extends Controller
{
    public function index(Request $request){
    	require_once base_path("/vendor/vgrem/php-spo/vendor/autoload.php");
    	$Url = "https://artba.sharepoint.com/sites/artbanet";
		try {
			$authCtx = new AuthenticationContext($Url);
			$authCtx->acquireTokenForUser($request->email,$request->password); //authenticate
			$ctx = new ClientContext($Url,$authCtx);

			echo "ok";

			//var_dump($ctx);
		}
		catch (Exception $e) {
			echo 'Authentication failed: ',  $e->getMessage(), "\n";
		}
    }

    public function signin(Request $request){
    	if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    		echo "ok";
        } else {
        	// email exists
        	if(Users::where('email', $request->email)->count() == 1){
        		DB::table('users')->where('email', $request->email)->update([
					"password" => bcrypt($request->password)
				]);
        	} else {
        		DB::table('users')->insert([
					"email" => $request->email,
					"password" => bcrypt($request->password)
				]);
        	}
        	echo "ok";
        }

    }
}
