<?php

namespace App\Http\Controllers\Analytics;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;
use Auth;
use Google;

class ProjectController extends Controller
{
	public function index(){
		$data = [
    		"title" => "Analytics Overview",
    		"page" => "analytics",
    		"subpage" => NULL
    	];

    	return view('analytics.web.index', $data);
	}

    public function project($project){
        $title = $project;
    	if($project == "sctpp"){
    		$title = "Safety Certification for Transportation Project Professionals";
    	}
    	elseif($project == "olc"){
    		$title = "Online Learning Center";
    	}

    	$data = [
    		"title" => $title,
    		"page" => "analytics",
    		"subpage" => $project,
    		"project" => strtoupper($project),
            "data" => Google::trafficData()
    	];

    	return view('analytics.web.project-index', $data);
    }
}
