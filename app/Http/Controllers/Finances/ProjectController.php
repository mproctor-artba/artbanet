<?php

namespace App\Http\Controllers\Finances;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;
use Auth;
use Stripe;

class ProjectController extends Controller
{
	public function index(){
		$data = [
    		"title" => "Finances Overview",
    		"page" => "finances",
    		"subpage" => NULL,
    		"transactions" => Stripe::transactions()
    	];

    	return view('finances.index', $data);
	}

    public function project($project){
    	if($project == "sctpp"){
    		$title = "Safety Certification for Transportation Project Professionals";
    	}
    	elseif($project == "olc"){
    		$title = "Online Learning Center";
    	}

    	$data = [
    		"title" => $title,
    		"page" => "finances",
    		"subpage" => $project,
    		"project" => strtoupper($project),
    		"transactions" => Stripe::transactions()
    	];

    	return view('finances.project-index', $data);
    }
}
