<?php

namespace App\Services\Stripe;

use Illuminate\Support\Facades\Facade;

class StripeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Stripe';
    }
}