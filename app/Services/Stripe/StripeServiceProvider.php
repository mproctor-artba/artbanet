<?php

namespace App\Services\Stripe;

use Illuminate\Support\ServiceProvider;

class StripeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Stripe', function($app) {
            return new Stripe();
        });
    }
}