<?php

namespace App\Services\Google;

use Illuminate\Support\Facades\Facade;

class GoogleFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Google';
    }
}