<?php

namespace App\Services\Google;

use Illuminate\Support\ServiceProvider;

class GoogleServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Google', function($app) {
            return new Google();
        });
    }
}