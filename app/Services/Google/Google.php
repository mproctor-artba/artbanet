<?php

namespace App\Services\Google;

require_once(base_path("vendor/autoload.php"));


use Analytics;
use Spatie\Analytics\Period;

class Google
{
	public function trafficData(){
		
		//retrieve visitors and pageview data for the current day and the last seven days
		$analyticsData = Analytics::fetchVisitorsAndPageViews(Period::days(7));

		//retrieve visitors and pageviews since the 6 months ago
		$analyticsData = Analytics::fetchVisitorsAndPageViews(Period::months(6));

		//retrieve sessions and pageviews with yearMonth dimension since 1 year ago 
		$analyticsData = Analytics::performQuery(
		    Period::years(1),
		    'ga:sessions',
		    [
		        'metrics' => 'ga:sessions, ga:pageviews',
		        'dimensions' => 'ga:yearMonth'
		    ]
		);

		return $analyticsData;
	}
}

